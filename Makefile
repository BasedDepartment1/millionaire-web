migrate:
	python backend/manage.py migrate $(if $m, api $m,)

makemigrations:
	python backend/manage.py makemigrations
	sudo chown -R ${USER} backend/app/migrations/

createsuperuser:
	python backend/manage.py createsuperuser

collectstatic:
	python backend/manage.py collectstatic --no-input

dev:
	python backend/manage.py runserver localhost:8000

command:
	python backend/manage.py ${c}

shell:
	python backend/manage.py shell

debug:
	python backend/manage.py debug

lint:
	isort .
	flake8 --config setup.cfg
	black --config pyproject.toml .

check_lint:
	isort --check --diff .
	flake8 --config setup.cfg
	black --check --config pyproject.toml .
