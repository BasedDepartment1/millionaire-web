# millionaire-web

Веб-версия игры «Кто хочет стать миллионером?» с возможностью выбора подсказок и лидербордом

| Главный экран | Режим игры | Десктопная версия |
|:-------------:|:----------:|:-----------------:|
| <img src="https://i.ibb.co/3TfYJph/photo-5407113704404801792-y.jpg" width="150"> | <img src="https://i.ibb.co/QC6kyWP/image-2024-06-13-16-04-34.png" width="150"> |  <img src="https://i.ibb.co/jgvySVF/photo-5407113704404801793-y.jpg" width="400">|

## Запуск

1. Установите Python

2. Установите зависимости <br/>
```
cd backend && pip install -r requirements.txt
```

3. Запустите dev-сервер с помощью make <br />
```
make dev
```

## Требования к проекту

- [X] На стартовом экране есть кнопка «Начать игру»
- [X] На экране игры показывается вопрос и 4 варианта ответа.
- [X] На финальном экране игры нужно показать, что игра окончена и вывести счет за игру. Еще нужна возможность как-то начать игру заново, не перезагружая страницу.
- [X] За одну игровую сессию может быть задано не больше 15 вопросов. Если на каком-то вопросе игрок ошибся или не уложился в отведенное время, то игра останавливается после этого вопроса.
- [X] Когда показывается вопрос, на экране идет таймер — дать ответ нужно успеть за 30 секунд. Если таймер окончился, а ответа не выбрано, подвсечивается правильный ответ и игра заканчивается
- [X] Если пользователь выбирает неправильный ответ, то игра показывает, подсвечивая зеленым, какой ответ был правильный (важно выбор игрока тоже как-то показывать, чтобы он знал, что правильный такой, а выбрал он другой). Правильный ответ показывается какое-то время, чтобы успеть его прочитать, не сразу завершение игры отображать.
- [X] Если пользователь выбрал правильный ответ, нужно подсветить его зеленым и переключить на следующий вопрос.
- [X] Если закончилось 15 вопросов и пользователь на все ответил правильно, нужно вывести на экран поздравление и итоговый счет
- [X] Подсказки. Кроме вопроса и вариантов ответа, на экране отображаются кнопки подсказок: 50/50 и Звонок другу
  - [X] Подсказка 50/50. При нажатии на эту кнопку, два неправильных варианта убираются
  - [X] Подсказка Звонок другу. При нажатии на эту кнопку, на экране появляется «совет от друга», который советует случайный пункт, с несколько повышенной вероятностью выбрать правильный ответ.
  - [X] **Вероятность правильного выбора друга зависит от сложности вопроса
- [X] Лидерборд. После окончания игры или в начале пользователь вводит свое имя. Когда он закончил игру, его результат добавляется в единый между всеми игроками лидерборд. Можно сравнивать себя с другими игроками.
- [X] *Реализовать счет за игру также, как в оригинальной игре: разная стоимость вопросов и несгораемые суммы.
- [X] *Вопросов должно быть написано больше 15 и они должны быть ранжированы по сложности и 15 из них должны выбираться случайным образом для каждой игры. Например, первые три вопроса — простые и для них есть 6 вариантов простых вопросов, из которых вопросы для конкретной игровой сессии выбираются случайно.
- [X] *Подсказка «помощь зала». После нажатия на эту кнопку, строится столбчатая диаграмма того, как якобы проголосовал зал. В этой диаграмме может быть любое распределение голосов, но правильный ответ должен перевешивать в любом случае.
  - [X] **Вероятность того, что зал выберет правильный ответ зависит от сложности вопроса. Для простых вопросов почти весь зал выбирает правильно. Для сложных зал может ошибиться.

