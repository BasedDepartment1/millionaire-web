import datetime
import random
from typing import List, Optional

from app.models import Game, Player, Question

QUESTIONS_PER_LEVEL = 5


def create_game(player_name: Optional[str]) -> Game:
    question_ids = [
        *_get_questions_with_complexity('easy'),
        *_get_questions_with_complexity('medium'),
        *_get_questions_with_complexity('hard'),
    ]
    game = Game(
        player_name=player_name,
        question_ids=question_ids
    )
    game.save()
    return game


def _get_questions_with_complexity(complexity: str) -> List[int]:
    ids = Question.objects.filter(
        complexity__exact=complexity,
    ).values('id')
    sample_len = min(len(ids), QUESTIONS_PER_LEVEL)
    return random.sample([q['id'] for q in ids], sample_len)


def get_correct_answer(
        game: Game,
        question: Question,
        answer_index: int | None,
) -> int:
    if question.correct_answer == answer_index:
        game.total_questions += 1
        game.save()
    return question.correct_answer


def finish_game(game: Game, total_score: int):
    game.total_score = total_score
    game.finish_timestamp = datetime.datetime.now()
    game.status = 'finished'
    game.save()

    if not game.player_name:
        return

    player, _ = Player.objects.get_or_create(
        name=game.player_name
    )
    player.score = total_score
    player.last_game_id = game.id
    player.last_game_timestamp = game.finish_timestamp
    player.save()
