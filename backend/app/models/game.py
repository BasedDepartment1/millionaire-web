from django.contrib.postgres.fields import ArrayField
from django.db import models

__all__ = ['Game', ]


class Game(models.Model):
    status = models.CharField(choices=[
        ('in_progress', 'In Progress'),
        ('finished', 'Finished')
    ], blank=False, default='in_progress', db_index=True)
    total_score = models.IntegerField(blank=False, default=0)
    total_questions = models.IntegerField(blank=False, default=0)
    player_name = models.CharField(max_length=20, blank=True, null=True)
    question_ids = ArrayField(
        models.IntegerField(), max_length=15, default=list
    )
    finish_timestamp = models.DateTimeField(blank=True, null=True)


class Meta:
    verbose_name = 'Game'
    verbose_name_plural = 'Games'
