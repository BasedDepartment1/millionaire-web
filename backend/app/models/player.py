from django.db import models

__all__ = ['Player', ]


class Player(models.Model):
    name = models.CharField(
        max_length=20, blank=False, null=False,
        db_index=True, unique=True
    )
    score = models.IntegerField(default=0, db_index=True)
    last_game_timestamp = models.DateTimeField(null=True, blank=True)
    last_game_id = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return f'Player {self.name}'


class Meta:
    verbose_name = 'Player'
    verbose_name_plural = 'Players'
