from django.contrib.postgres.fields import ArrayField
from django.db import models

__all__ = ['Question', ]


class Question(models.Model):
    complexity = models.CharField(
        choices=[('easy', 'Easy'), ('medium', 'Medium'), ('hard', 'Hard')],
        db_index=True, blank=False
    )
    body = models.TextField(blank=False)
    choices = ArrayField(
        models.CharField(max_length=64), size=4, blank=False
    )
    correct_answer = models.IntegerField(choices=[
        (0, '1'), (1, '2'), (2, '3'), (3, '4')
    ], blank=False)

    def __str__(self):
        return f'Question #{self.id} ({self.complexity})'


class Meta:
    verbose_name = 'Question'
    verbose_name_plural = 'Questions'
