import app.models
import django.core.validators
from app.millionaire import games
from app.util import api_errors, rest_util, serializers
from django.db.models import F
from rest_framework.request import Request

DEFAULT_TABLE_SIZE = 10


@rest_util.endpoint_get
@rest_util.as_json
@api_errors.exception_means_500
def get_hello_message(request: Request):
    return {
        'message': 'Hello, World!',
    }


@rest_util.endpoint_get
@rest_util.as_json
@api_errors.exception_means_500
@api_errors.not_found_means_404
def get_game_info(request: Request, game_id: int):
    game = app.models.Game.objects.get(pk=game_id)
    return serializers.serialize_game(game)


@rest_util.endpoint_post
@rest_util.as_json
@api_errors.exception_means_500
def create_game(request: Request):
    player_name = request.data.get('player_name', None)
    new_game = games.create_game(player_name)
    return serializers.serialize_game(new_game)


@rest_util.endpoint_post
@rest_util.as_json
@api_errors.exception_means_500
@api_errors.validation_error_means_422
@api_errors.not_found_means_404
def check_answer(request: Request, game_id: int):
    question_no = request.data['question_no']
    answer = request.data['answer']

    if answer is not None:
        rest_util.range_validator(1, 4)(answer)

    game = app.models.Game.objects.get(pk=game_id)

    rest_util.range_validator(1, len(game.question_ids))(question_no)

    question = app.models.Question.objects.get(
        pk=game.question_ids[question_no - 1]
    )

    correct_answer = games.get_correct_answer(
        game,
        question,
        answer - 1 if answer else None
    )

    return {
        'correct_answer': correct_answer + 1,
        'success': correct_answer + 1 == answer,
    }


@rest_util.endpoint_post
@rest_util.as_json
@api_errors.not_found_means_404
def finish_game(request: Request, game_id: int):
    game = app.models.Game.objects.get(pk=game_id)

    games.finish_game(game, request.data['total_score'])

    return {
        'status': 'finished'
    }


@rest_util.endpoint_get
@rest_util.as_json
@api_errors.exception_means_500
@api_errors.validation_error_means_422
def get_top_scorers(request: Request):
    qs_count = request.query_params.get('count', DEFAULT_TABLE_SIZE)

    django.core.validators.validate_integer(qs_count)
    count = int(qs_count)

    django.core.validators.MinValueValidator(
        1, 'Count must be positive integer'
    )(count)

    return (
        app.models.Player.objects
        .order_by('-score', '-last_game_timestamp', '-name')
        .values(player_name=F('name'), total_score=F('score'))[:count:1]
        # app.models.Game.objects
        # .exclude(player_name__isnull=True)
        # .exclude(player_name__exact='')
        # .filter(status__exact='finished')
        # .order_by('-total_score', '-finish_timestamp')
        # .values('player_name', 'total_score')[:count:1]
    )
