import app.rest
from django.urls import path

urlpatterns = [
    path(r'hello', app.rest.get_hello_message, name='hello'),
    path(r'game/<int:game_id>', app.rest.get_game_info),
    path(r'game/new', app.rest.create_game),
    path(r'game/top', app.rest.get_top_scorers),
    path(r'game/<int:game_id>/validate', app.rest.check_answer),
    path(r'game/<int:game_id>/finish', app.rest.finish_game),
]
