import functools
from http import HTTPStatus

from django.core.exceptions import ValidationError
from django.db.models import ObjectDoesNotExist


def _return_error_code_for_error(error_class, return_code):
    def decorator(fn):
        @functools.wraps(fn)
        def wrapper(*args, **kwargs):
            try:
                return fn(*args, **kwargs)
            except error_class as e:
                return {'error': repr(e)}, return_code
        return wrapper
    return decorator


def exception_means_500(fn):
    return _return_error_code_for_error(
        Exception, HTTPStatus.INTERNAL_SERVER_ERROR
    )(fn)


def not_found_means_404(fn):
    return _return_error_code_for_error(
        ObjectDoesNotExist, HTTPStatus.NOT_FOUND
    )(fn)


def validation_error_means_422(fn):
    return _return_error_code_for_error(
        ValidationError, HTTPStatus.UNPROCESSABLE_ENTITY
    )(fn)
