import functools

import django.core.validators
import rest_framework.decorators
from django.http import JsonResponse


def range_validator(min_border: int, max_border: int):
    def validate(num):
        django.core.validators.MinValueValidator(min_border)(num)
        django.core.validators.MaxValueValidator(max_border)(num)

    return validate


def _use_endpoint_with_method(method: str):
    def decorator(fn):
        @functools.wraps(fn)
        def wrapper(*args, **kwargs):
            return rest_framework.decorators.api_view([method])(
                fn
            )(*args, **kwargs)

        return wrapper

    return decorator


def as_json(fn):
    @functools.wraps(fn)
    def wrapper(*args, **kwargs):
        raw_response = fn(*args, **kwargs)
        match raw_response:
            case dict():
                return JsonResponse(raw_response)
            case list():
                return JsonResponse(raw_response, safe=False)
            case (data, status_code):
                return JsonResponse(
                    data=data, status=status_code
                )
            case _:
                raise ValueError(f'Unexpected result: {raw_response}')

    return wrapper


def endpoint_get(fn):
    return _use_endpoint_with_method('GET')(fn)


def endpoint_post(fn):
    return _use_endpoint_with_method('POST')(fn)


def endpoint_put(fn):
    return _use_endpoint_with_method('PUT')(fn)


def endpoint_delete(fn):
    return _use_endpoint_with_method('DELETE')(fn)
