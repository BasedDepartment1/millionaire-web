import app.models
from django.forms.models import model_to_dict


def serialize_game(game: app.models.Game) -> dict:
    raw_game = model_to_dict(game)
    question_ids = raw_game.pop('question_ids')
    actual_questions = [
        app.models.Question.objects.values(
            'id', 'complexity', 'body', 'choices'
        ).get(pk=id_)
        for id_ in question_ids
    ]

    return {
        **raw_game,
        'questions': actual_questions
    }
