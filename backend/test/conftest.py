import pytest
from rest_framework.test import APIClient


@pytest.fixture(scope='session', autouse=True)
def client():
    return APIClient()
