from test.util import TestBase

import app.models
import pytest
from hamcrest import assert_that, equal_to

test_base = TestBase(__file__)
fixtures = test_base.get_fixtures()

COMPLEXITY_LEVELS = [
    'easy',
    'medium',
    'hard',
]


@pytest.fixture
def basic_game():
    basic_game = fixtures['basic_game.yaml']
    prepare_db(basic_game)


@pytest.fixture
def no_games():
    no_games = fixtures['no_games.yaml']
    prepare_db(no_games)


@pytest.fixture
def finished_games():
    finished_games = fixtures['finished_games.yaml']
    prepare_db(finished_games)


@pytest.fixture
def game_answers():
    game_answers = fixtures['game_answers.yaml']
    prepare_db(game_answers)


@pytest.fixture
def games_in_progress():
    games_in_progress = fixtures['games_in_progress.yaml']
    prepare_db(games_in_progress)


def prepare_db(data):
    for question in data.get('questions', []):
        app.models.Question.objects.create(**question)
    for game in data.get('games', []):
        app.models.Game.objects.create(**game)
    for player in data.get('players', []):
        app.models.Player.objects.create(**player)


@pytest.mark.django_db
@pytest.mark.usefixtures('basic_game')
@test_base.use_cases('get_game.yaml')
def test_get_game(client, id, expected):
    res = client.get(f'/api/game/{id}')
    assert_that(res.status_code, equal_to(expected['status_code']))
    assert_that(res.json(), equal_to(expected['game']))


@pytest.mark.django_db
@pytest.mark.usefixtures('no_games')
@test_base.use_cases('create_game.yaml')
def test_create_game(client, name, question_counts, expected_game):
    data = {}
    if name is not None:
        data['player_name'] = name

    res = client.post('/api/game/new', data=data)
    assert_that(res.status_code, equal_to(200))
    actual_game = res.json()
    questions = actual_game['questions']

    for complexity_level in COMPLEXITY_LEVELS:
        assert_that(
            len([q for q in questions if q['complexity'] == complexity_level]),
            equal_to(question_counts[complexity_level])
        )

    assert_that(actual_game, equal_to(expected_game))

    db_game = app.models.Game.objects.first()
    assert_that(
        len(db_game.question_ids),
        equal_to(sum(question_counts.values()))
    )
    assert_that(db_game.player_name, equal_to(name))
    # raise Exception(f'{db_game.player_name=}')


@pytest.mark.django_db
@pytest.mark.usefixtures('finished_games')
@test_base.use_cases('get_top_scorers.yaml')
def test_get_top_scorers(client, count, status_code, expected):
    url = '/api/game/top'
    if count is not None:
        url += f'?count={count}'
    resp = client.get(url)
    assert_that(resp.status_code, equal_to(status_code))
    assert_that(resp.json(), equal_to(expected))


@pytest.mark.django_db
@pytest.mark.usefixtures('game_answers')
@test_base.use_cases('validate_questions.yaml')
def test_validate_questions(client, existent_id, answers, expected):
    for answer in answers:
        expected_result = answer.pop('correct', False)
        expected_status_code = answer.pop('status_code', None)

        resp = client.post(
            f'/api/game/{answer.pop("game_id")}/validate',
            answer, format='json'
        )

        assert_that(resp.status_code, equal_to(expected_status_code))
        assert_that(
            resp.json().get('success', False),
            equal_to(expected_result)
        )

    if not existent_id:
        return

    actual_game = app.models.Game.objects.values('total_questions').get(
        pk=existent_id
    )

    assert_that(actual_game, equal_to(expected))


@pytest.mark.django_db
@pytest.mark.usefixtures('games_in_progress')
@test_base.use_cases('finish_game.yaml')
def test_finish_game(client, game_id, payload, expected):
    resp = client.post(f'/api/game/{game_id}/finish', payload, format='json')

    assert_that(resp.status_code, equal_to(expected['status_code']))

    if expected['total_score'] is None:
        return

    finished_game = app.models.Game.objects.values(
        'total_score', 'finish_timestamp'
    ).get(pk=game_id)

    assert_that(finished_game['finish_timestamp'] is not None)

    assert_that(
        finished_game['total_score'],
        equal_to(expected['total_score'])
    )
