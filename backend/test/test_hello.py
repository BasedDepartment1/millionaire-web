from hamcrest import assert_that, equal_to


def test_get_hello_message(client):
    resp = client.get('/api/hello')
    assert_that(resp.status_code, equal_to(200))
    assert_that(resp.json(), equal_to({'message': 'Hello, World!'}))
