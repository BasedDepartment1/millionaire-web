import os
import pathlib
from typing import Iterable, List

import pytest
import yaml


def _get_yaml_data(path: str | pathlib.Path) -> dict | list:
    with open(path, 'r') as f:
        return yaml.safe_load(f)


def _replace_any(case):
    if not isinstance(case, dict):
        return
    for key, value in case.items():
        if value == 'ANY':
            case[key] = AnyValue()
        _replace_any(value)


def _process_case(case_values: Iterable) -> List:
    final_values = list(case_values)
    for value in final_values:
        _replace_any(value)
    return final_values


class TestBase:
    def __init__(self, base_test_file: str | pathlib.Path):
        base = os.path.dirname(base_test_file)
        self._cases_path = pathlib.Path(base, 'cases')
        self._fixtures_path = pathlib.Path(base, 'fixtures')

    def use_cases(self, cases_file_name: str | pathlib.Path):
        cases_data = _get_yaml_data(pathlib.Path(
            self._cases_path, cases_file_name
        ))
        cases = cases_data['cases']
        argnames = cases[0].keys()
        values = [_process_case(case.values()) for case in cases]
        return pytest.mark.parametrize(','.join(argnames), values)

    def get_fixtures(self) -> dict[str, dict | list]:
        fixtures = dict()
        for fixture_file in self._fixtures_path.iterdir():
            if not fixture_file.is_file() or fixture_file.suffix != '.yaml':
                raise ValueError(
                    f'{fixture_file} should not be placed '
                    f'in directory called "fixtures", '
                    f'only .yaml-files are supported'
                )
            fixture_data = _get_yaml_data(fixture_file)
            fixtures[fixture_file.name] = fixture_data

        return fixtures


class AnyValue:
    def __eq__(self, other):
        return True

    def __repr__(self):
        return '<ANY>'

    def __str__(self):
        return repr(self)
