export const BACKEND_PATH = '';

export const env = 'pages';

export const mockedLeaderboard = [
    {
        'player_name': 'prikotletka',
        'total_score': 1000000
    },
    {
        'player_name': 'svayp11',
        'total_score': 750000
    },
    {
        'player_name': 'MiniTapok',
        'total_score': 500000
    },
    {
        'player_name': 'Юля',
        'total_score': 400000
    },
    {
        'player_name': 'Вова',
        'total_score': 300000
    },
    {
        'player_name': 'Глеб',
        'total_score': 250000
    },
    {
        'player_name': 'Собачка',
        'total_score': 125000
    },
    {
        'player_name': 'Мужчина',
        'total_score': 64000
    },
    {
        'player_name': 'Женщина',
        'total_score': 32000
    },
    {
        'player_name': 'dr0pDead',
        'total_score': 0
    }
];
