import millionaire from './millionaire.js';
import GameView from './GameView.js';
import {biasedDistribution, biasedPick, sample} from './rand.js';


const SCORE_LEVELS = [
    0,
    100,
    200,
    300,
    500,
    1000,
    2000,
    4000,
    8000,
    16000,
    32000,
    64000,
    125000,
    250000,
    500000,
    1000000,
];

const SECONDS_PER_QUESTION = 30;
const MILLISECONDS_TO_WAIT = 1000;
const MILLISECONDS_TO_NEXT = 1000;


function getStartQuestionState() {
    return {
        success: false,
        gameOver: false,
        pauseTimer: false,
        secondsRemaining: SECONDS_PER_QUESTION,
    };
}

function sleep(ms) {
    return new Promise(resolve => {
        setTimeout(resolve, ms);
    });
}


export class Game {
    constructor(gameId, playerName, questions) {
        this.view = new GameView();

        this.gameId = gameId;
        this.questions = questions;
        this.playerName = playerName;

        this.currentScore = 0;
        this.currentQuestionIdx = 0;

        this.used = {
            fiftyFifty: false,
            friendCall: false,
            audienceHelp: false,
        };

        this.questionState = null;
        this.view.activateGameTabs();
        this.#renderNextQuestion();
    }

    static async fromBackend(playerName) {
        const {
            id: gameId,
            questions,
        } = await millionaire.game.create(playerName);

        return new Game(gameId, playerName, questions);
    }

    get currentSavedScore() {
        const scoreIndex = Math.trunc(this.currentQuestionIdx / 5) * 5;
        return SCORE_LEVELS[scoreIndex];
    }

    async finish(isSuccess, manualExit = false) {
        this.questionState.gameOver = true;

        const finishingScore = (isSuccess || manualExit)
            ? this.currentScore
            : this.currentSavedScore;
        await millionaire.game.finish(this.gameId, finishingScore);

        this.view.showResultsWindow(
            this.playerName,
            finishingScore,
        );
    }

    async pickAnswer(answerNo) {
        this.#pauseTimer();

        this.view.lockAnswerButtons();
        this.view.highlightPicked(answerNo);
        const {
            correct_answer: correctAnswerNo,
            success: isCorrect,
        } = await millionaire.game.validateQuestion(
            this.gameId,
            this.currentQuestionIdx + 1,
            answerNo,
        );

        if (isCorrect) {
            await this.#handleSuccess(answerNo);
        } else {
            await this.#handleFailure(answerNo, correctAnswerNo);
        }

        await sleep(MILLISECONDS_TO_WAIT + MILLISECONDS_TO_NEXT);
        this.view.unlockAnswerButtons();
    }

    async run() {
        while (!this.questionState.gameOver) {
            if (this.questionState.success) {
                await sleep(MILLISECONDS_TO_NEXT);
                this.questionState = getStartQuestionState();
                this.view.removeAllHighlighting();
                this.view.renderHintModes(this.used);
                this.#renderNextQuestion();
                this.view.unlockAnswerButtons();
            }

            if (this.questionState.pauseTimer) {
                await sleep(1000);
                continue;
            }

            if (this.questionState.secondsRemaining <= 0) {
                await this.#failForSkippedQuestion();
            }

            this.questionState.secondsRemaining = Math.max(
                0, this.questionState.secondsRemaining - 1,
            );
            this.view.renderTimeRemaining(this.questionState.secondsRemaining);
            await sleep(1000);
        }
    }

    async pickHint(name) {
        if (this.used[name]) {
            return;
        }
        this.used[name] = true;

        this.view.activateHint(name);
        this.#pauseTimer();

        const {
            correct_answer: correctAnswerNo,
        } = await millionaire.game.validateQuestion(
            this.gameId,
            this.currentQuestionIdx + 1,
            null,
        );

        const questionComplexity
            = this.questions[this.currentQuestionIdx].complexity;

        if (name === 'fiftyFifty') {
            this.processFiftyFifty(correctAnswerNo);
        } else if (name === 'friendCall') {
            this.processFriendCall(correctAnswerNo, questionComplexity);
        } else if (name === 'audienceHelp') {
            this.processAudienceHelp(correctAnswerNo, questionComplexity);
        }

        this.#continueTimer();
    }

    processFiftyFifty(correctAnswerNo) {
        const wrongAnswers = [1, 2, 3, 4].filter(
            el => el !== correctAnswerNo,
        );
        const answersToDrop = sample(wrongAnswers, 2);
        this.view.disableAnswers(answersToDrop);
    }

    processFriendCall(correctAnswerNo, complexity) {
        const friendThought = biasedPick(
            correctAnswerNo,
            complexity,
        );
        this.view.highlightHinted(friendThought);
    }

    processAudienceHelp(correctAnswerNo, complexity) {
        const percentages = biasedDistribution(
            correctAnswerNo,
            complexity,
        );
        this.view.showVotes(percentages);
    }

    async #failForSkippedQuestion() {
        this.#pauseTimer();
        this.view.lockAnswerButtons();

        const {
            correct_answer: correctAnswer,
        } = await millionaire.game.validateQuestion(
            this.gameId,
            this.currentQuestionIdx + 1,
            null,
        );

        this.view.highlightCorrect(correctAnswer);
        await sleep(MILLISECONDS_TO_NEXT + MILLISECONDS_TO_WAIT);

        this.view.removeAllHighlighting();
        await this.finish(false);
    }

    #renderNextQuestion() {
        const {
            body: nextQuestion,
            choices,
        } = this.questions[this.currentQuestionIdx];

        this.view.renderQuestion(nextQuestion, choices);
        this.questionState = getStartQuestionState();
    }

    async #handleSuccess(answerNo) {
        await sleep(MILLISECONDS_TO_WAIT);

        this.currentQuestionIdx++;
        this.currentScore = SCORE_LEVELS[this.currentQuestionIdx];
        this.view.highlightCorrect(answerNo);

        await sleep(MILLISECONDS_TO_NEXT);

        this.view.renderScore(this.currentScore);
        if (this.currentQuestionIdx !== 15) {
            this.questionState.success = true;
        } else {
            await this.finish(true);
        }
    }

    async #handleFailure(givenAnswerNo, correctAnswerNo) {
        await sleep(MILLISECONDS_TO_WAIT);
        this.view.highlightIncorrect(givenAnswerNo, correctAnswerNo);

        await sleep(MILLISECONDS_TO_NEXT);
        await this.finish(false);
    }

    #pauseTimer() {
        this.questionState.pauseTimer = true;
    }

    #continueTimer() {
        this.questionState.pauseTimer = false;
    }
}

export default Game;
