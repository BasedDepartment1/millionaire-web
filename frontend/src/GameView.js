import {prettifyScore} from './util.js';


const PICKED_CLASS = 'answer__container__overlay_picked';
const CORRECT_CLASS = 'answer__container__overlay_right';
const WRONG_CLASS = 'answer__container__overlay_wrong';
const HINTED_CLASS = 'answer__container__overlay_help';
const HIDDEN_CLASS = 'answer__container__overlay_hidden';

export class GameView {
    constructor() {
        this.score = document.getElementById('score');
        this.timer = document.getElementById('time');

        this.questionBody = document.getElementById('question');
        this.choices = document.getElementsByClassName('answer__text');
        this.choiceContainers = document.getElementsByClassName(
            'answer__container__overlay',
        );

        this.resultsWindow = document.getElementById('results-window');
        this.resultsWindowScore = document.getElementById('score-final');

        this.hints = {
            fiftyFifty: document.getElementById('half-options'),
            friendCall: document.getElementById('phone-call'),
            audienceHelp: document.getElementById('options-chart'),
        };

        this.percentages = document.getElementsByClassName(
            'histogram-percentage',
        );

    }

    renderQuestion(body, choices) {
        this.questionBody.innerHTML = body;

        for (let i = 0; i < 4; ++i) {
            this.choices[i].innerHTML = choices[i];
        }
    }

    renderScore(score) {
        this.score.innerHTML = prettifyScore(score);
    }

    renderTimeRemaining(timeRemaining) {
        this.timer.innerHTML = timeRemaining;

        if (timeRemaining <= 5) {
            this.colorTimer();
        }
    }

    colorTimer() {
        this.timer.classList.add('time__text_running-out');
    }

    showResultsWindow(playerName, totalScore) {
        console.log(`${playerName || 'Unknown player'} ended the game`);
        this.resultsWindowScore.innerText = prettifyScore(totalScore);
        this.resultsWindow.classList.remove('hidden-modal');
        this.resultsWindow.classList.add('blur-bg');
        this.deactivateGameTabs();
    }

    lockAnswerButtons() {
        for (const container of this.choiceContainers) {
            container.classList.remove('click_available');
            container.classList.add('click_unavailable');
        }
    }

    unlockAnswerButtons() {
        for (const container of this.choiceContainers) {
            container.classList.remove('click_unavailable');
            container.classList.add('click_available');
        }
    }

    highlightPicked(answerNo) {
        this.choiceContainers[answerNo - 1].classList.add(
            PICKED_CLASS,
        );
    }

    highlightCorrect(answerNo) {
        this.choiceContainers[answerNo - 1].classList.remove(
            HINTED_CLASS,
        );
        this.choiceContainers[answerNo - 1].classList.add(
            CORRECT_CLASS,
        );
    }

    highlightIncorrect(givenAnswerNo, correctAnswerNo) {
        this.highlightCorrect(correctAnswerNo);
        this.choiceContainers[givenAnswerNo - 1].classList.remove(
            HINTED_CLASS,
        );
        this.choiceContainers[givenAnswerNo - 1].classList.add(
            WRONG_CLASS,
        );
    }

    highlightHinted(answerNo) {
        this.choiceContainers[answerNo - 1].classList.add(
            HINTED_CLASS,
        );
    }

    removeAllHighlighting() {
        for (const container of this.choiceContainers) {
            container.classList.remove(
                WRONG_CLASS,
                CORRECT_CLASS,
                PICKED_CLASS,
                HINTED_CLASS,
                HIDDEN_CLASS,
            );
        }
        this.clearVotes();
        this.timer.classList.remove('time__text_running-out');
    }

    activateHint(type) {
        this.hints[type].classList.add('game-button_focused');

        const otherHints = Object.entries(this.hints)
            .filter(([key]) => key !== type);

        for (const obj of otherHints) {
            obj[1].classList.add('click_unavailable');
            obj[1].tabIndex = -1;
        }
    }

    disableAnswers(answerNumbers) {
        for (const num of answerNumbers) {
            this.choiceContainers[num - 1].classList.add(
                HIDDEN_CLASS,
            );
        }
    }

    showVotes(percentages) {
        for (let i = 0; i < 4; ++i) {
            this.percentages[i].innerText = `${percentages[i]}%`;
        }
    }

    clearVotes() {
        for (let i = 0; i < 4; ++i) {
            this.percentages[i].innerText = '';
        }
    }

    renderHintModes(used) {
        for (const [key, wasUsed] of Object.entries(used)) {
            const hintView = this.hints[key];
            if (wasUsed) {
                hintView.classList.remove('game-button_focused');
                hintView.disabled = true;
            } else {
                hintView.classList.remove('click_unavailable');
                hintView.tabIndex = 0;
            }
        }
    }

    deactivateGameTabs() {
        this.#switchGameTabs(false);
    }

    activateGameTabs() {
        this.#switchGameTabs(true);
    }

    #switchGameTabs(activate) {
        const index = activate ? 0 : -1;
        this.questionBody.tabIndex = index;
        for (const choice of this.choiceContainers) {
            choice.tabIndex = index;
        }
        for (const hint of Object.values(this.hints)) {
            hint.tabIndex = index;
        }
    }
}

export default GameView;