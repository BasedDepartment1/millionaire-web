import {BACKEND_PATH} from '../env/current/env.js';


export class HTTPMethod {
    static GET = 'GET';
    static POST = 'POST';
    static PUT = 'PUT';
    static DELETE = 'DELETE';
}


class Client {
    constructor() {
        this.backendPath = BACKEND_PATH;
    }

    async invoke(
        endpoint = '/',
        method = HTTPMethod.GET,
        data = {},
        headers = {},
    ) {
        if (!endpoint || endpoint.charAt(0) !== '/') {
            endpoint = `/${endpoint}`;
        }

        const url = (method === HTTPMethod.GET && Object.keys(data).length > 0)
            ? this.#formatQueryString(endpoint, data)
            : `${this.backendPath}${endpoint}`;

        console.log(`Requesting data from ${url}`);

        return await fetch(
            url, this.#buildInit(method, data, headers),
        ).then(resp => resp.json())
            .catch(reason => console.error(`Error while fetching: ${reason}`));
    }

    #buildInit(method, data, headers) {
        const baseInit = {
            method: method,
            headers: {
                'Content-Type': 'application/json',
                ...headers,
            },
            redirect: 'follow',
        };

        if (method !== HTTPMethod.GET) {
            baseInit.body = JSON.stringify(data);
        }

        return baseInit;
    }

    #formatQueryString(endpoint, data) {
        const querystringParams = [];
        for (const [key, value] of Object.entries(data)) {
            querystringParams.push(`${key}=${value}`);
        }

        return `${this.backendPath}${endpoint}?${querystringParams.join('&')}`;
    }
}

export const client = new Client();

export default client;