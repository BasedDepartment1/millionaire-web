import Game from './Game.js';


const playerName = window.sessionStorage.getItem('playerName');
const game = await Game.fromBackend(playerName);

const answerContainers = document.getElementsByClassName(
    'answer__container__overlay',
);
for (let i = 0; i < 4; ++i) {
    answerContainers[i].addEventListener(
        'click',
        game.pickAnswer.bind(game, i + 1),
    );
}
document.getElementById('exit-button').addEventListener(
    'click',
    game.finish.bind(game, false, true),
);
document.getElementById('half-options').addEventListener(
    'click',
    game.pickHint.bind(game, 'fiftyFifty'),
);
document.getElementById('phone-call').addEventListener(
    'click',
    game.pickHint.bind(game, 'friendCall'),
);
document.getElementById('options-chart').addEventListener(
    'click',
    game.pickHint.bind(game, 'audienceHelp'),
);

document.addEventListener('keydown',  e => {
    if (e.code === 'Space' || e.code === 'Enter') {
        document.activeElement.click();
    }
});

await game.run();
