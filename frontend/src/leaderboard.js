import millionaire from './millionaire.js';
import {env} from '../env/current/env.js';
import {prettifyScore} from './util.js';

async function fillLeaderboard() {
    const games = (env === 'pages')
        ? await getMockedBoard()
        : await millionaire.game.getTopScorers();

    const table = document.getElementById('leaderboard');
    games.forEach(game => {
        const {
            player_name: playerName,
            total_score: totalScore,
        } = game;
        insertPlayerScore(table, playerName, totalScore);
    });
}

function insertPlayerScore(table, playerName, totalScore) {
    const row = table.insertRow();

    const name = row.insertCell(0);
    name.innerHTML = playerName;
    name.className = 'leaderboard__table_column1';

    const score = row.insertCell(1);
    score.innerHTML = prettifyScore(totalScore);
    score.className = 'leaderboard__table_column2';
}


async function getMockedBoard() {
    const {
        mockedLeaderboard,
    } = await import('../env/current/env.js');
    
    return mockedLeaderboard;
}

function storeName() {
    const playerName = document.getElementById('username').value;
    window.sessionStorage.setItem('playerName', playerName);
}


document.getElementById('play-button').addEventListener(
    'click', storeName,
);

document.getElementById('username-form').addEventListener(
    'submit', storeName,
);

fillLeaderboard();
