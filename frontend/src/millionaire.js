import client, {HTTPMethod} from './client.js';

export const millionaire = {
    game: {
        get: async function getGame(gameId) {
            return await client.invoke(
                `/game/${gameId}`,
                HTTPMethod.GET,
            );
        },
        create: async function newGame(playerName = null) {
            return await client.invoke(
                '/game/new',
                HTTPMethod.POST,
                {
                    'player_name': playerName,
                },
            );
        },
        getTopScorers: async function top(count = 10) {
            return await client.invoke(
                '/game/top',
                HTTPMethod.GET,
                {
                    'count': count,
                },
            );
        },
        validateQuestion: async function validate(gameId, questionNo, answer) {
            return await client.invoke(
                `/game/${gameId}/validate`,
                HTTPMethod.POST,
                {
                    'question_no': questionNo,
                    'answer': answer,
                },
            );
        },
        finish: async function finishGame(gameId, totalScore) {
            return await client.invoke(
                `/game/${gameId}/finish`,
                HTTPMethod.POST,
                {
                    'total_score': totalScore,
                },
            );
        },
    },
};

export default millionaire;