export function sample(collection, count) {
    const shuffled = collection.slice(0);
    let i = collection.length;
    while (i--) {
        const index = Math.floor((i + 1) * Math.random());
        const temp = shuffled[index];
        shuffled[index] = shuffled[i];
        shuffled[i] = temp;
    }
    return shuffled.slice(0, count);
}

export function biasedPick(correctNo, complexity) {
    const randomNum = Math.random();
    let threshold;

    if (complexity === 'easy') {
        threshold = 0.9;
    } else if (complexity === 'medium') {
        threshold = 0.7;
    } else if (complexity === 'hard') {
        threshold = 0.5;
    }

    if (randomNum < threshold) {
        return correctNo;
    }

    const filteredChoices = [1, 2, 3, 4].filter(el => el !== correctNo);
    const randomIndex = Math.floor(Math.random() * filteredChoices.length);
    return filteredChoices[randomIndex];
}

export function biasedDistribution(correctNo, complexity) {
    let correctPercentage = 0;
    if (complexity === 'easy') {
        correctPercentage = truncToPercent(normalRandom(75, 10));
    } else if (complexity === 'medium') {
        correctPercentage = truncToPercent(normalRandom(50, 15));
    } else if (complexity === 'hard') {
        correctPercentage = truncToPercent(normalRandom(30, 20));
    }

    const remaining = distributeRemaining(3, 100 - correctPercentage);
    remaining.splice(correctNo - 1, 0, correctPercentage);
    return remaining;
}


function normalRandom(mean, stdDev) {
    const u = Math.random();
    const v = Math.random();
    const randStdNormal = Math.sqrt(-2 * Math.log(u))
        * Math.sin(2 * Math.PI * v);
    return Math.round(mean + (stdDev * randStdNormal));
}

function truncToPercent(val) {
    return Math.max(
        0, Math.min(
            100,
            Math.round(val),
        ));
}

function distributeRemaining(length, remaining) {
    const arr = Array.from(
        { length }, () => Math.floor(Math.random() * 100),
    );
    const sum = arr.reduce((acc, val) => acc + val, 0);
    const scalingFactor = remaining / sum;
    const result = arr.map((val) => Math.round(val * scalingFactor));

    const sumResult = result.reduce((acc, val) => acc + val, 0);
    if (sumResult !== remaining) {
        const diff = remaining - sumResult;
        result[result.length - 1] += diff;
    }
    return result;
}