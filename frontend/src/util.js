export function prettifyScore (score) {
    return new Intl.NumberFormat('ru-RU', {
        maximumFractionDigits: 0,
    }).format(score).replaceAll(/\s/g, ' ');
}